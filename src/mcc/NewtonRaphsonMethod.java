package mcc;

/**
 * Implementación del método de Newton-Raphson.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class NewtonRaphsonMethod {
    
    /**
     * La función a la cual se aplica el método (f: R -> R).
     */
    private Function<Double, Double> function;
    
    /**
     * La derivada de la función.
     */
    private Function<Double, Double> derivative;
    
    /**
     * Constructor vacío; se debe establecer la función y su derivada
     * posteriormente, antes de aplicar el método, de lo contrario se lanzará
     * una excepción.
     */
    public NewtonRaphsonMethod() {
        // Constructor vacío
    }
    
    /**
     * Constructor con la función y su derivada.
     * 
     * @param f la función
     * @param d la derivada de la función
     */
    public NewtonRaphsonMethod(Function<Double, Double> f, Function<Double, Double> d) {
        this.function = f;
        this.derivative = d;
    }
    
    /**
     * Intenta encontrar una raíz de la función a partir del valor dado.
     * 
     * @param x0 el valor inicial
     * @param e el error para finalizar
     * @return la raíz de la función, si fue encontrada
     * @throws RuntimeException si la función no fue establecida
     */
    public double apply(double x0, double e)
            throws RuntimeException {
        double x1, y1;
        
        if (function == null || derivative == null) {
            throw new IllegalStateException("Función/derivada no establecida");
        }
        
        do {
            x1 = x0 - (f(x0)/d(x0));
            y1 = f(x1);
            x0 = x1;
        } while (y1 != 0.0 && Math.abs(y1) >= e);
        
        return x1;
    }
    
    /**
     * Evalúa la derivada con el valor dado (d(x)) y devuelve su resultado.
     * 
     * @param x el valor a evaluar
     * @return el resultado
     */
    public Double d(Double x) {
        return derivative.evaluate(x);
    }
    
    /**
     * Evalúa la función con el valor dado (f(x)) y devuelve su resultado.
     * 
     * @param x el valor a evaluar
     * @return el resultado
     */
    public Double f(Double x) {
        return function.evaluate(x);
    }
    
    /**
     * Devuelve la derivada de la función establecida.
     * 
     * @return la derivada de la función
     */
    public Function<Double, Double> getDerivative() {
        return derivative;
    }
    
    /**
     * Establece la derivada de la función para la aplicación del método.
     * 
     * @param f la derivada de la función
     */
    public void setDerivative(Function<Double, Double> d) {
        this.derivative = d;
    }
    
    /**
     * Devuelve la función establecida.
     * 
     * @return la función
     */
    public Function<Double, Double> getFunction() {
        return function;
    }
    
    /**
     * Establece la función para la aplicación del método.
     * 
     * @param f la función
     */
    public void setFunction(Function<Double, Double> f) {
        this.function = f;
    }
}
