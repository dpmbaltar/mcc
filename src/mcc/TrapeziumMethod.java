package mcc;

/**
 * Implementación del método de los Trapecios.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class TrapeziumMethod {
    
    /**
     * La función a la cual se aplica el método (f: R -> R).
     */
    private Function<Double, Double> function;
    
    /**
     * Puntos donde se aplica el método.
     */
    private Point2D[] points;
    
    /**
     * Constructor vacío; se debe establecer la función o un arreglo de puntos
     * posteriormente, antes de aplicar el método, de lo contrario se lanzará
     * una excepción. Si se establecieron ambos, se utiliza preferentemente la
     * función.
     */
    public TrapeziumMethod() {
        // Constructor vacío
    }
    
    /**
     * Constructor con una función.
     * 
     * @param f la función
     */
    public TrapeziumMethod(Function<Double, Double> f) {
        this.function = f;
    }
    
    /**
     * Constructor con un arreglo de puntos.
     * 
     * @param points el arreglo de puntos
     */
    public TrapeziumMethod(Point2D[] points) {
        this.points = points;
    }
    
    /**
     * Aplica el método desde a hasta b, con 1 área elemental.
     * 
     * @param a valor izquierdo del intervalo
     * @param b valor derecho del intervalo
     * @return el área
     */
    public double apply(double a, double b) {
        return apply(a, b, 1);
    }
    
    /**
     * Aplica el método desde a hasta b, con n áreas elementales.
     * Si se utilizan puntos en vez de una función, n será igual a la cantidad
     * de puntos menos 1, es decir: <code>points.length - 1</code>
     * 
     * @param a valor izquierdo del intervalo
     * @param b valor derecho del intervalo
     * @param n la cantidad de áreas elementales
     * @return el área
     * @throws IllegalStateException si no se estableció la función o puntos
     */
    public double apply(double a, double b, int n) throws RuntimeException {
        double h, sum = 0.0, area = 0.0;
        double[] x;
        
        if (function == null && points == null) {
            throw new IllegalStateException("Función/puntos no establecidos");
        }
        
        // Se utiliza preferentemente la función
        if (function != null) {
            n = Math.abs(n);
            h = (b - a)/n;
        } else {
            n = points.length - 1;
            h = points[1].getX() - points[0].getX();
        }
        
        x = new double[n - 1];
        
        if (n > 1) {
            for (int i = 0; i < (n - 1); i++) {
                x[i] = f(a+(h*(i+1)));
                sum+= x[i];
            }
        }
        
        area = (h/2)*(f(a)+f(b)+2*sum);
        printDetails(a, b, n, h, x, area);
        
        return area;
    }
    
    /**
     * Evalúa la función con el valor dado (f(x)) y devuelve su resultado.
     * 
     * @param x el valor a evaluar
     * @return el resultado
     * @throws IllegalStateException si no se estableció la función o puntos
     * @throws RuntimeException si el valor de f(x) no está definido
     */
    public Double f(Double x) throws RuntimeException {
        double y = 0.0;
        
        if (function != null) {
            y = function.evaluate(x);
        } else if (points != null) {
            boolean found = false;
            for (int i = 0; i < points.length; i++) {
                if (x == points[i].getX()) {
                    y = points[i].getY();
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new RuntimeException("f(x) no está definido para x = "+x);
            }
        } else {
            throw new IllegalStateException("Función/puntos no establecidos");
        }
        
        return y;
    }
    
    public void printDetails(double a, double b, int n, double h, double[] x, double area) {
        System.out.println("h = ("+b+" - "+a+")/"+n+" = "+h);
        System.out.print("A = ("+h+"/2)*("+f(a));
        if (n > 1) {
            System.out.print(" + 2*(");
            for (int i = 0; i < x.length; i++) {
                System.out.print(x[i]);
                if (i < (x.length - 1)) {
                    System.out.print(" + ");
                }
            }
            System.out.print(")");
        }
        System.out.println(" + "+f(b)+")");
        System.out.println("A = "+area);
    }
}
