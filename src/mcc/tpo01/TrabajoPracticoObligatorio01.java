package mcc.tpo01;

import mcc.Function;
import mcc.NewtonRaphsonMethod;
import mcc.Point2D;
import mcc.SimpsonOneThirdMethod;

/**
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class TrabajoPracticoObligatorio01 {
    
    public static void main(String[] args) {
        ejercicio1();
        ejercicio2();
    }
    
    public static void ejercicio1() {
        final double error = 0.001;
        double root;
        Function<Double, Double> f, d;
        NewtonRaphsonMethod nrm;
        
        // d(t) = 80 + 90*cos((Pi/3)*t)
        f = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double t) {
                return 80 + 90*Math.cos((Math.PI/3)*t);
            }
        };
        // d'(t) = -30*Pi*sen((Pi/3)*t)
        d = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double t) {
                return -30*Math.PI*Math.sin((Math.PI/3)*t);
            }
        };
        
        nrm = new NewtonRaphsonMethod(f, d);
        root = nrm.apply(4, error);
        
        System.out.println(root);
    }
    
    public static void ejercicio2() {
        double p;
        SimpsonOneThirdMethod sotm;
        Point2D[] points = {
                new Point2D(0, 71736000),
                new Point2D(10, 63700000),
                new Point2D(20, 52920000),
                new Point2D(30, 47040000),
                new Point2D(40, 34300000),
                new Point2D(50, 18620000),
                new Point2D(60, 0)
        };
        
        sotm = new SimpsonOneThirdMethod(points);
        p = sotm.apply(0, 60);
        
        System.out.println(p);
    }
}
