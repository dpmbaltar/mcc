package mcc;

/**
 * Implementación del método de la bisección.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class BisectionMethod {
    
    /**
     * La función a la cual se aplica el método (f: R -> R).
     */
    private Function<Double, Double> function;
    
    /**
     * Constructor vacío; se debe establecer la función posteriormente, antes
     * de aplicar el método, de lo contrario se lanzará una excepción.
     */
    public BisectionMethod() {
        // Constructor vacío
    }
    
    /**
     * Constructor con la función a bisectar como parámetro.
     * 
     * @param f la función a bisectar
     */
    public BisectionMethod(Function<Double, Double> f) {
        this.function = f;
    }
    
    /**
     * Bisecta la función según los parámetros dados.
     * 
     * @param a el extremo izquierdo del intervalo
     * @param b el extremo derecho del intervalo
     * @param e el error para finalizar
     * @return la raíz de la función, si fue encontrada
     * @throws RuntimeException si la función no fue establecida
     */
    public double apply(double a, double b, double e)
            throws RuntimeException {
        double fa, fb, fr, r;
        boolean bisect, isRoot;
        
        if (function == null) {
            throw new IllegalStateException("Función no establecida");
        }
        
        r = 0.0;
        fa = f(a);
        fb = f(b);
        bisect = fa*fb < 0;
        isRoot = false;
        
        if (!bisect) {
            throw new IllegalArgumentException("Error: f(a)*f(b) >= 0");
        }
        
        // Bisecta mientras que f(a)*f(b) < 0, f(r) < e y f(r) != 0
        while (bisect && !isRoot) {
            r = (a + b)/2;
            fr = f(r);
            
            if (fr == 0.0 || Math.abs(fr) < e) {
                isRoot = true;
            } else if (fa*fr < 0) {
                b = r;
            } else if (fr*fb < 0) {
                a = r;
            } else {
                bisect = false;
            }
        }
        
        return r;
    }
    
    /**
     * Evalúa la función con el valor dado (f(x)) y devuelve su resultado.
     * 
     * @param x el valor a evaluar
     * @return el resultado
     */
    public Double f(Double x) {
        return (Double) function.evaluate(x);
    }
    
    /**
     * Devuelve la función establecida.
     * 
     * @return la función
     */
    public Function<Double, Double> getFunction() {
        return function;
    }
    
    /**
     * Establece la función para la aplicación del método.
     * 
     * @param f la función
     */
    public void setFunction(Function<Double, Double> f) {
        this.function = f;
    }
}
