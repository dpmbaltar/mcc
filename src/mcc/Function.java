package mcc;

/**
 * Interfaz genérica para una función.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 * 
 * @param <DomainType> el típo del dominio
 * @param <CodomainType> el típo del codominio
 */
public interface Function<DomainType, CodomainType> {
    
    /**
     * Devuelve el resultado de evaluar la función en el valor dado.
     * 
     * @param x variable de la función
     * @return el resultado
     */
    public CodomainType evaluate(DomainType x);
}
