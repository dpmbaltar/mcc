package mcc;

/**
 * Método numérico abstracto.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
abstract public class AbstractNumericMethod {
    
    /**
     * La función a la cual se aplica el método (f: R -> R).
     */
    protected Function<Double, Double> function;
    
    /**
     * Constructor con una función como parámetro.
     * 
     * @param f la función
     */
    protected AbstractNumericMethod(Function<Double, Double> f) {
        this.function = f;
    }
    
    /**
     * Evalúa la función con el valor dado (f(x)) y devuelve su resultado.
     * 
     * @param x el valor a evaluar
     * @return el resultado
     */
    abstract public Double f(Double x);
    
    /**
     * Devuelve la función establecida.
     * 
     * @return la función
     */
    public Function<Double, Double> getFunction() {
        return function;
    }
    
    /**
     * Establece la función para la aplicación del método.
     * 
     * @param f la función
     */
    public void setFunction(Function<Double, Double> f) {
        this.function = f;
    }
}
