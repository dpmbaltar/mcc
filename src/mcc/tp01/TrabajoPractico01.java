package mcc.tp01;

import mcc.BisectionMethod;
import mcc.Function;
import mcc.NewtonRaphsonMethod;
import mcc.Point2D;
import mcc.SecantMethod;
import mcc.SimpsonOneThirdMethod;
import mcc.TrapeziumMethod;

/**
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class TrabajoPractico01 {
    
    /**
     * Funciones de R en R.
     */
    private static Function<Double, Double> c, d;
    
    /**
     * Derivadas de funciones.
     */
    private static Function<Double, Double> d1;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Ejercicio 14
        // f(x) = 80*exp(-2*x) + 20*exp((-1/2)*x) - 7
        d = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 80*Math.exp(-2*x)+20*Math.exp(-0.5*x)-7;
            }
        };
        // f'(x) = -160*exp(-2*x) - 10*exp((-1/2)*x) - 7
        d1 = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return -160*Math.exp(-2*x)-10*Math.exp(-0.5*x);
            }
        };
        
        testBisectionMethod();
        testNewtonRaphsonMethod();
        testSecantMethod();
        //testE18A();
        //testE19A();
        //testE19B();
    }
    
    public static void testBisectionMethod() {
        final double error = 0.1;
        double x0, x1, x2, x3, x4;
        Function<Double, Double> a, b, c;
        BisectionMethod bm = new BisectionMethod();
        
        // f(x) = x^3 - 2*x - 1
        a = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return Math.pow(x, 3) - 2*x - 1;
            }
        };
        // f(x) = 2*sen(x+1)
        b = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 2*Math.sin(x + 1);
            }
        };
        // f(x) = 7*sen(x)*exp(-x) - 1
        c = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 7*Math.sin(x)*Math.exp(-x) - 1;
            }
        };
        
        try {
            bm.setFunction(a);
            x0 = bm.apply(-1.2, -0.8, error);
            x1 = bm.apply(-0.8, -0.4, error);
            x2 = bm.apply(1, 2, error);
            bm.setFunction(b);
            x3 = bm.apply(1, Math.PI, error);
            
            System.out.println("Prueba ejercicio 7 (Bisección)");
            System.out.println("f(x) = x^3 - 2*x - 1:");
            System.out.println("Raíz aprox. en [-1.2, -0.8]: "+x0);
            System.out.println("Raíz aprox. en [-0.8, -0.4]: "+x1);
            System.out.println("Raíz aprox. en [1, 2]: "+x2);
            System.out.println("f(x) = 2*sen(x + 1)");
            System.out.println("Raíz aprox. en [1, Pi]: "+x3);
            System.out.println();
            
            bm.setFunction(c);
            x4 = bm.apply(1, 3, error);
            System.out.println("Prueba ejercicio 12 (Bisección)");
            System.out.println("f(x) = 7*sen(x)*exp(-x) - 1:");
            System.out.println("Raíz aprox. en [1, 3]: "+x4);
            System.out.println();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
    
    public static void testNewtonRaphsonMethod() {
        final double error = 0.1;
        double x0, x1, x2, x3, x4;
        Function<Double, Double> a, b, c, ad, bd, cd;
        NewtonRaphsonMethod nrm = new NewtonRaphsonMethod();
        
        // f(x) = x^3 - 2*x - 1
        a = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return Math.pow(x, 3) - 2*x - 1;
            }
        };
        // f'(x) = 3*x^2 - 2
        ad = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 3*Math.pow(x, 2) - 2;
            }
        };
        // f(x) = 2*sen(x + 1)
        b = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 2*Math.sin(x + 1);
            }
        };
        // f'(x) = 2*cos(x + 1)
        bd = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 2*Math.cos(x + 1);
            }
        };
        // f(x) = 7*sen(x)*exp(-x) - 1
        c = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 7*Math.sin(x)*Math.exp(-x) - 1;
            }
        };
        // f'(x) = 7*exp(-x)*(cos(x) - sen(x))
        cd = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 7*Math.exp(-x)*(Math.cos(x) - Math.sin(x));
            }
        };
        
        try {
            nrm.setFunction(a);
            nrm.setDerivative(ad);
            x0 = nrm.apply(-1.2, error);
            x1 = nrm.apply(-0.4, error);
            x2 = nrm.apply(2, error);
            nrm.setFunction(b);
            nrm.setDerivative(bd);
            x3 = nrm.apply(1, error);
            
            System.out.println("Prueba ejercicio 7 (Newton-Raphson)");
            System.out.println("f(x) = x^3 - 2*x - 1:");
            System.out.println("Raíz aprox. con x0 = -1.2: "+x0);
            System.out.println("Raíz aprox. con x0 = -0.4: "+x1);
            System.out.println("Raíz aprox. con x0 = 2: "+x2);
            System.out.println("f(x) = 2*sen(x + 1)");
            System.out.println("Raíz aprox. con x0 = 2: "+x3);
            System.out.println();
            
            nrm.setFunction(c);
            nrm.setDerivative(cd);
            x4 = nrm.apply(1, error);
            
            System.out.println("Prueba ejercicio 12 (Newton-Raphson)");
            System.out.println("f(x) = 7*sen(x)*exp(-x) - 1:");
            System.out.println("Raíz aprox. con x0 = 1: "+x4);
            System.out.println();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
    
    public static void testSecantMethod() {
        final double error = 0.1;
        double x0, x1, x2, x3, x4;
        Function<Double, Double> a, b, c;
        SecantMethod sm = new SecantMethod();
        
        // f(x) = x^3 - 2*x - 1
        a = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return Math.pow(x, 3) - 2*x - 1;
            }
        };
        // f(x) = 2*sen(x + 1)
        b = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 2*Math.sin(x + 1);
            }
        };
        // f(x) = 7*sen(x)*exp(-x) - 1
        c = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 7*Math.sin(x)*Math.exp(-x) - 1;
            }
        };
        
        try {
            sm.setFunction(a);
            x0 = sm.apply(-1.2, -0.8, error);
            x1 = sm.apply(-0.8, -0.4, error);
            x2 = sm.apply(1, 2, error);
            sm.setFunction(b);
            x3 = sm.apply(1, Math.PI, error);
            
            System.out.println("Prueba ejercicio 7 (Secante)");
            System.out.println("f(x) = x^3 - 2*x - 1:");
            System.out.println("Raíz aprox. en [-1.2, -0.8]: "+x0);
            System.out.println("Raíz aprox. en [-0.8, -0.4]: "+x1);
            System.out.println("Raíz aprox. en [1, 2]: "+x2);
            System.out.println("f(x) = 2*sen(x + 1)");
            System.out.println("Raíz aprox. en [1, Pi]: "+x3);
            System.out.println();
            
            sm.setFunction(c);
            x4 = sm.apply(1, 3, error);
            
            System.out.println("Prueba ejercicio 12 (Secante)");
            System.out.println("f(x) = 7*sen(x)*exp(-x) - 1:");
            System.out.println("Raíz aprox. en [1, 3]: "+x4);
            System.out.println();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
    
    public static void testE18A() {
        TrapeziumMethod tm;
        Function<Double, Double> f;
        
        f = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return 5*Math.pow(x, 2) + 2*x - 3;
            }
        };
        
        tm = new TrapeziumMethod(f);
        tm.apply(2, 4, 4);
        tm.apply(2, 4, 8);
        
        f = new Function<Double, Double>() {
            @Override
            public Double evaluate(Double x) {
                return Math.exp(x);
            }
        };
        
        tm = new TrapeziumMethod(f);
        tm.apply(2.8, 5.2, 4);
        tm.apply(2.8, 5.2, 8);
    }
    
    public static void testE19A() {
        TrapeziumMethod tm;
        Point2D[] points = {
                new Point2D(2.0, 0.2239),
                new Point2D(2.5, -0.0484),
                new Point2D(3.0, -0.2601),
                new Point2D(3.5, -0.3801),
                new Point2D(4.0, -0.387)
                };
        
        tm = new TrapeziumMethod(points);
        tm.apply(2, 4);
    }
    
    public static void testE19B() {
        SimpsonOneThirdMethod sotm;
        Point2D[] points = {
                new Point2D(2.0, 0.2239),
                new Point2D(2.5, -0.0484),
                new Point2D(3.0, -0.2601),
                new Point2D(3.5, -0.3801),
                new Point2D(4.0, -0.387)
                };
        
        sotm = new SimpsonOneThirdMethod(points);
        System.out.println(sotm.apply(2, 4));
    }
}
