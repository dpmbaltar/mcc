package mcc;

/**
 * Implementación del método de Simpson 1/3.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class SimpsonOneThirdMethod {
    
    /**
     * La función a la cual se aplica el método (f: R -> R).
     */
    private Function<Double, Double> function;
    
    /**
     * Puntos donde se aplica el método.
     */
    private Point2D[] points;
    
    /**
     * Constructor vacío; se debe establecer la función o un arreglo de puntos
     * posteriormente, antes de aplicar el método, de lo contrario se lanzará
     * una excepción. Si se establecieron ambos, se utiliza preferentemente la
     * función.
     */
    public SimpsonOneThirdMethod() {
        // Constructor vacío
    }
    
    /**
     * Constructor con una función.
     * 
     * @param f la función
     */
    public SimpsonOneThirdMethod(Function<Double, Double> f) {
        this.function = f;
    }
    
    /**
     * Constructor con un arreglo de puntos.
     * 
     * @param points el arreglo de puntos
     */
    public SimpsonOneThirdMethod(Point2D[] points) {
        this.points = points;
    }
    
    /**
     * Aplica el método desde a hasta b, con 2 área elementales.
     * 
     * @param a valor izquierdo del intervalo
     * @param b valor derecho del intervalo
     * @return el área
     */
    public double apply(double a, double b) {
        return apply(a, b, 2);
    }
    
    /**
     * Aplica el método desde a hasta b, con n áreas elementales.
     * Si se utilizan puntos en vez de una función, n será igual a la cantidad
     * de puntos menos 1, es decir: <code>points.length - 1</code>
     * 
     * @param a valor izquierdo del intervalo
     * @param b valor derecho del intervalo
     * @param n la cantidad de áreas elementales (debe ser par)
     * @return el área
     * @throws IllegalStateException si no se estableció la función o puntos
     * @throws IllegalStateException si n es impar
     */
    public double apply(double a, double b, int n) throws RuntimeException {
        double area = 0.0, h, evenSum = 0.0, oddSum = 0.0;
        
        if (function == null && points == null) {
            throw new IllegalStateException("Función/puntos no establecidos");
        } else if ((n % 2) != 0 || (points.length % 2) == 0) {
            throw new IllegalStateException("Áreas elementales impar");
        }
        
        // Se utiliza preferentemente la función
        if (function != null) {
            n = Math.abs(n);
            h = (b - a)/n;
        } else {
            n = points.length - 1;
            h = points[1].getX() - points[0].getX();
        }
        
        // Sumar xi, donde 1 <= i <= (m - 1), m = cantidad(points) , & i es impar
        for (int i = 0; i <= (points.length - 1); i++) {
            if ((i % 2) == 1) {
                oddSum+= f(a + (h*i));
            }
        }
        
        // Sumar xi, donde 2 <= i <= (m - 2), m = cantidad(points) , & i es par
        for (int i = 2; i <= (points.length - 2); i++) {
            if ((i % 2) == 0) {
                evenSum+= f(a + (h*i));
            }
        }
        
        area = (h/3)*(f(a) + (4*oddSum) + (2*evenSum) + f(b));
        
        return area;
    }
    
    /**
     * Evalúa la función con el valor dado (f(x)) y devuelve su resultado.
     * 
     * @param x el valor a evaluar
     * @return el resultado
     * @throws IllegalStateException si no se estableció la función o puntos
     * @throws RuntimeException si el valor de f(x) no está definido
     */
    public Double f(Double x) throws RuntimeException {
        double y = 0.0;
        
        if (function != null) {
            y = function.evaluate(x);
        } else if (points != null) {
            boolean found = false;
            for (int i = 0; i < points.length; i++) {
                if (x == points[i].getX()) {
                    y = points[i].getY();
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new RuntimeException("f(x) no está definido para x = "+x);
            }
        } else {
            throw new IllegalStateException("Función/puntos no establecidos");
        }
        
        return y;
    }
    
    /**
     * Devuelve la función establecida.
     * 
     * @return la función
     */
    public Function<Double, Double> getFunction() {
        return function;
    }
    
    /**
     * Establece la función para la aplicación del método.
     * 
     * @param f la función
     */
    public void setFunction(Function<Double, Double> f) {
        this.function = f;
    }
    
    /**
     * Devuelve los puntos establecidos.
     * 
     * @return el arreglo de puntos
     */
    public Point2D[] getPoints() {
        return points;
    }
    
    /**
     * Establece los puntos para la aplicación del método.
     * 
     * @param points el arreglo de puntos
     */
    public void setFunction(Point2D[] points) {
        this.points = points;
    }
}
