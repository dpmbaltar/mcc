package mcc.parcial;

import mcc.Function;
import mcc.SecantMethod;

public class Parcial1 {

	public static void main(String[] args) {
		ejercicio1();
	}
	
	public static void ejercicio1() {
		final double error = 0.01;
		double root;
		Function<Double, Double> fn;
		SecantMethod sm;
		
		fn = new Function<Double, Double>() {
			@Override
			public Double evaluate(Double x) {
				return 7*(2 - Math.pow(0.9, x)) - 10;
			}
		};
		
		sm = new SecantMethod(fn);
		root = sm.findRoot(5, 7, error);
		
		System.out.println("f(x) = 7*(2 - 0.9^x) - 10");
		System.out.println("r = "+root);
		System.out.println("f(r) = "+sm.f(root));
	}
}
