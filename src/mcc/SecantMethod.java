package mcc;

/**
 * Implementación del método de la Secante.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class SecantMethod {
    
    /**
     * La función a la cual se aplica el método (f: R -> R).
     */
    private Function<Double, Double> function;
    
    /**
     * Constructor vacío; se debe establecer la función posteriormente, antes
     * de aplicar el método, de lo contrario se lanzará una excepción.
     */
    public SecantMethod() {
        // Constructor vacío
    }
    
    /**
     * Constructor con la función.
     * 
     * @param f la función
     */
    public SecantMethod(Function<Double, Double> f) {
        this.function = f;
    }
    
    /**
     * Intenta encontrar una raíz de la función a partir de los valores dados.
     * 
     * @param x0 el primer valor
     * @param x1 el segundo valor
     * @param e el error para finalizar
     * @return la raíz de la función, si fue encontrada
     * @throws RuntimeException si la función no fue establecida
     */
    public double apply(double x0, double x1, double e)
            throws RuntimeException {
        double x2, y2;
        
        if (function == null) {
            throw new IllegalStateException("Función no establecida");
        }
        
        do {
            x2 = x1 - (f(x1)*(x1 - x0)/(f(x1) - f(x0)));
            y2 = f(x2);
            x0 = x1;
            x1 = x2;
        } while (y2 != 0.0 && Math.abs(y2) >= e);
        
        return x2;
    }
    
    /**
     * Evalúa la función con el valor dado (f(x)) y devuelve su resultado.
     * 
     * @param x el valor a evaluar
     * @return el resultado
     */
    public Double f(Double x) {
        return function.evaluate(x);
    }
    
    /**
     * Devuelve la función establecida.
     * 
     * @return la función
     */
    public Function<Double, Double> getFunction() {
        return function;
    }
    
    /**
     * Establece la función para la aplicación del método.
     * 
     * @param f la función
     */
    public void setFunction(Function<Double, Double> f) {
        this.function = f;
    }
}
