package mcc;

/**
 * Implementación de un punto en dos dimensiones.
 * 
 * @author Diego P. M. Baltar <dpmbaltar@gmail.com>
 */
public class Point2D implements Comparable<Point2D> {
    
    /**
     * El valor de x.
     */
    private final double x;
    
    /**
     * El valor de y.
     */
    private final double y;
    
    /**
     * Constructor con los valores de x e y.
     * 
     * @param x el valor de x
     * @param y el valor de y
     */
    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Devuelve el valor de x.
     * 
     * @return el valor de x
     */
    public double getX() {
        return x;
    }
    
    /**
     * Devuelve el valor de y.
     * 
     * @return el valor de y
     */
    public double getY() {
        return y;
    }
    
    /**
     * Crea y devuelve un nuevo punto con los mismos valores de x e y.
     * 
     * @return el nuevo punto
     */
    public Point2D copy() {
        return new Point2D(x, y);
    }
    
    @Override
    public boolean equals(Object o) {
        return o instanceof Point2D
                ? ((Point2D) o).getX() == x && ((Point2D) o).getY() == y
                : false;
    }
    
    @Override
    public String toString() {
        return "("+x+", "+y+")";
    }
    
    @Override
    public int compareTo(Point2D point) {
        int result = 0;
        
        if (x >= point.getX() && y > point.getY()) {
            result = 1;
        } else if (x <= point.getX() && y < point.getY()) {
            result = -1;
        }
        
        return result;
    }
}
